﻿namespace VehicleFramework.ActorFramework.Graphics
{
    public enum ConditionDrawMode
    {
        Pristine,
        Weathered,
        Damaged,
        Destroyed,
    }
}