﻿using UnityEngine;
using VehicleFramework.ActorFramework.Defs;
using Verse;

namespace VehicleFramework.ActorFramework.Trackers
{
    public class ActorStoryTracker : IExposable
    {
        public Color bodyColor;
        public BodyTypeDef bodyType;
        public void ExposeData()
        {
            Scribe_Defs.Look(ref bodyType, "bodyType");
        }
    }
}