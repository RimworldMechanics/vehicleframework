﻿using Verse;

namespace VehicleFramework.Components
{
    public class CompProperties_Motorized : CompProperties
    {
        public CompProperties_Motorized()
        {
            compClass = typeof (CompMotorized);
        }
    }
}