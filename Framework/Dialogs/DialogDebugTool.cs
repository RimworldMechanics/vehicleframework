﻿using System.Collections.Generic;
using RimWorld;
using RimWorld.Planet;
using VehicleFramework.Defs;
using Verse;

namespace VehicleFramework.Dialogs
{
    public class DialogDebugTool : Dialog_DebugOptionLister
    {
        protected override void DoListingItems()
        {
            DoGap();
            
            DoLabel("Vehicle framework - Development tool");
            
            if (Current.ProgramState == ProgramState.Playing)
            {
                if (WorldRendererUtility.WorldRenderedNow)
                {
                    // Options visible on world map
                } else if (Find.CurrentMap != null)
                {
                    // Options visible on generated map
                    DoListingItems_Map();
                }
            }
        }

        private void DoListingItems_Map()
        {
            Text.Font = GameFont.Tiny;
            DoLabel("Spawning");
            DebugAction("Vehicles", () =>
            {
                List<DebugMenuOption> debugMenuOptionList = new List<DebugMenuOption>();
                Log.Message("Loading vehicles");
                
                foreach (var vehicleDef in DefDatabase<VehicleDef>.AllDefs)
                {
                    Log.Message(vehicleDef.defName);
                    debugMenuOptionList.Add(new DebugMenuOption(vehicleDef.defName, DebugMenuOptionMode.Tool, () =>
                    {
                        var actorDef = DefDatabase<ThingDef>.GetNamed(vehicleDef.defName);
                        var thing = ThingMaker.MakeThing(actorDef);
                        GenSpawn.Spawn(thing, UI.MouseCell(), Find.CurrentMap);
                    }));
                }

                Find.WindowStack.Add(new Dialog_DebugOptionListLister(debugMenuOptionList));
            });
        }
    }
}