﻿using System.Reflection;
using Harmony;
using RimWorld;
using Verse;

namespace VehicleFramework
{
    public class VehicleFrameworkMod : Mod
    {
        [StaticConstructorOnStartup]
        class Main
        {
            static Main()
            {
                var harmony = HarmonyInstance.Create("com.github.harmony.rimworld.mod.VehicleFramework");
                harmony.PatchAll(Assembly.GetExecutingAssembly());
            }
        }
        
        public VehicleFrameworkMod(ModContentPack content) : base(content)
        {
      
        }
    }
}