# Vehicle Framework

This Rimworld mod is intended to provide a framework for other modders to easily add vehicles to the game.

## Contributing

All contributions are welcomed and encouraged. Make sure to open an issue or comment on an existing issue to let us know that you intend to work on something :)

## Source setup

If you are using Jetbrains rider as an IDE, then all you to do is procure the following dependencies:

    UnityEngine.dll from your game folder (.\RimWorld\RimWorldWin_Data\Managed)
    Assembly-CSharp.dll from your game folder (.\RimWorld\RimWorldWin_Data\Managed)
    0Harmony.dll by building the source in this repository.

We are using a bash script to copy the mod files and assemblies into the Rimworld folder. You can pass your rimworld installation path as a parameter of the script or set the RW_PATH environment variable to your installation path. If you do not have a bash shell you can use the one provided by Git for windows.

### As a command argument
```
$ ./Scripts/install.sh /d/Steam/steamapps/common/RimWorld
```

### As an environment variable
```
$ export RW_PATH=/d/Steam/steamapps/common/RimWorld
$ echo "RW_PATH=/d/Steam/steamapps/common/RimWorld" >> ~/.bashrc
$ ./Scripts/install.sh
```