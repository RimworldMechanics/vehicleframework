#!/bin/bash
cd ${0%/*}
RW_PATH="${RW_PATH}"
if [ -z "$RW_PATH" ] ; then RW_PATH=$1 ; fi
if [ -z "$RW_PATH" ] ; then echo "Must provide rimworld installation path"; exit 1 ; fi

mkdir -p ../Framework/Mod/Assemblies/
mkdir -p ../ExampleMod/Mod/Assemblies/
rm ../Framework/Mod/Assemblies/*
rm ../ExampleMod/Mod/Assemblies/*
cp --verbose -rf ../Framework/bin/Debug/0Harmony.dll ../Framework/Mod/Assemblies/0Harmony.dll
cp --verbose -rf ../Framework/bin/Debug/VehicleFramework.dll ../Framework/Mod/Assemblies/VehicleFramework.dll
cp --verbose -rf ../ExampleMod/bin/Debug/0Harmony.dll ../ExampleMod/Mod/Assemblies/0Harmony.dll
cp --verbose -rf ../ExampleMod/bin/Debug/ExampleMod.dll ../ExampleMod/Mod/Assemblies/ExampleMod.dll
rm -r "$RW_PATH/Mods/VehicleFramework"
mkdir "$RW_PATH/Mods/VehicleFramework"
rm -r "$RW_PATH/Mods/ExampleMod"
mkdir "$RW_PATH/Mods/ExampleMod"
cp --verbose -rf ../ExampleMod/Mod/* "$RW_PATH/Mods/ExampleMod"
cp --verbose -rf ../Framework/Mod/* "$RW_PATH/Mods/VehicleFramework"
